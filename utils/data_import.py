import os
import glob
import tensorflow as tf
import tensorflow.data.experimental as tfde

path = os.getcwd() + "/data/*.csv"


class DataImporter(object):

    def __init__(self):
        super(DataImporter, self).__init__()
        self.data_length = -1
        self.batch_size = 100
        self.column_names = ['open', 'high', 'low', 'close', 'volume', 'NDHighLow']
        self.feature_names = self.column_names[1:-1]
        self.label_name = self.column_names[-1]
        self.train_dataset = None

    def file_len(self, fname):
        with open(fname, mode="r") as f:
            i = 0
            for i, l in enumerate(f):
                pass
        return i + 1

    def data_size(self):
        files = glob.glob(path)
        lines = 0
        for f in files:
            lines = lines + (self.file_len(f) - 2)
        self.data_length = lines
        print("Total data length:" + str(self.data_length))
        pass

    def get_training_set(self):
        if self.train_dataset is None:
            self.data_size()
            self.train_dataset = tfde.make_csv_dataset(
                path,
                self.batch_size,
                select_columns=self.column_names,
                column_defaults=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                label_name=self.label_name,
                num_epochs=None,
                shuffle=True, shuffle_seed=910199, shuffle_buffer_size=self.batch_size * 10,
                prefetch_buffer_size=self.batch_size * 10,
                num_rows_for_inference=None,
                na_value="nan")

            def pack_features_vector(features, labels):
                """Pack the features into a single array."""
                features = tf.stack(list(features.values()), axis=1)
                return features, labels

        return self.data_length, self.train_dataset.map(pack_features_vector)
