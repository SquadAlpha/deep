import os
import sys
from datetime import datetime

import tensorflow.contrib.summary as tfs
#import tensorflow.contrib.summary.summary as tfs
from tensorflow.python.keras.callbacks import History
from tensorflow.python.ops.summary_ops_v2 import SummaryWriter

from utils.summary_callback import SummaryCallback


def save_model(model, hist: History, time):
    now = time.strftime("%m-%d_%H-%M")
    save_dir = "save/" + os.path.basename(sys.argv[0]) + "_" + now + "/"
    a = []

    def returnMe(y):
        a.append(y)

    model.summary(print_fn=returnMe)

    model.save(filepath=save_dir + "model.h5", overwrite=True, include_optimizer=False)
    file = open(save_dir + now + ".info.txt", mode='w')
    file.write(os.path.basename(sys.argv[0]) + "\r\n")
    file.write("in:" + str(model.input_shape) + "\r\n"
               + "\r\n".join(a) + "\r\n"
               + "out:" + str(model.output_shape) + "\r\n"
               + "time:" + datetime.now().strftime("%m-%d_%H-%M"))
    file.close()
    file = open(save_dir + now + ".history.txt", mode='w')
    file.write(os.path.basename(sys.argv[0]) + "\r\n")
    for k, v in hist.history.items():
        file.write("k:{}=v:{}\r\n".format(k, v))
    file.close()


def get_callbacks(model, time):
    now = time.strftime("%m-%d_%H-%M")
    logdir = "save/" + os.path.basename(sys.argv[0]) + "_" + now + "/logs"
    os.makedirs(logdir, exist_ok=True)
    file_writer: SummaryWriter = tfs.create_file_writer(logdir, flush_millis=1000)
    file_writer.set_as_default()
    km = SummaryCallback(file_writer, model, logdir)
    return [km]
