import csv
import os

import quandl
from datetime import datetime
from iexfinance.stocks import get_historical_data


def compute_direction(previous, current):
    if current > previous:
        return 2  # higher
    elif current < previous:
        return 0  # lower
    elif current == previous:
        return 1  # same
    else:
        raise ArithmeticError("If one number isn't smaller bigger or the same as the other it isn't a real number\r\n"
                              "Suspects:" + previous + " and " + current)


# make a clean dictionary containing cleanly defined values
def clean_row(dirty_dict, keys):
    clean_dict = {}
    for k in keys:
        clean_dict[k] = getattr(dirty_dict, k)
    return clean_dict


# writes all
def write_csv(file, data, headers):
    writer = csv.DictWriter(file, delimiter=',', quotechar='"', fieldnames=headers, quoting=csv.QUOTE_MINIMAL)
    writer.writeheader()
    for toup in data.itertuples(name="Row"):
        writer.writerow(clean_row(toup, headers))


def new_nd_column(data):
    data['NDHighLow'] = 0


def write_nd_column(data):
    new_nd_column(data)
    # go over each row and set the Target value for yesterday
    last = 0.0
    first = True
    for row in data.itertuples(name='Row'):
        if first:
            first = False
        else:
            data.at[row.Index, 'NDHighLow'] = compute_direction(last, data.at[row.Index, 'close'])
        last = data.at[row.Index, 'close']

    # shift the entire target value column one backwards
    data['NDHighLow'] = data['NDHighLow'].shift(-1)
    # remove last row, we can't predict the future (yet)
    data = data[:-1]
    return data


def fetch_quandl(quandl_name, fields=None, start_date="2003-01-02", **kwargs):
    if fields is None:
        fields = {}
    file_path = os.getcwd() + '/data/' + quandl_name.replace('/', '_') + '.csv'
    data = quandl.get(quandl_name, start_date=start_date, **kwargs)
    data = data.rename(index=str, columns=fields)
    data = write_nd_column(data)
    with open(file_path, mode='w') as dataFile:
        write_csv(dataFile, data, fields.values())


def fetch_iextrading(iextrading_name, start=datetime(2019 - 5, 1, 2), end=datetime.now(), target_column='close'):
    file_path = os.getcwd() + '/data/' + iextrading_name + '.csv'
    data = get_historical_data(iextrading_name, start, end, output_format='pandas')
    if target_column != 'close':
        data.rename(columns={target_column: 'close'}, inplace=True)
    data = write_nd_column(data)
    with open(file_path, mode='w') as dataFile:
        write_csv(dataFile, data, ['open', 'high', 'low', 'close', 'volume', 'NDHighLow'])


# set some standard vars
quandl.ApiConfig.api_key = os.environ['QUANDL_KEY']

# fetch data
fetch_quandl("FSE/VOW3_X",
             fields={'Open': 'open', 'High': 'high', 'Low': 'low', 'Close': 'close', 'Traded Volume': 'volume',
                     'NDHighLow': 'NDHighLow'},
             start_date="2003-01-02",
             # end_date="2018-12-31",
             qopts={'columns': ['Open', 'High', 'Low', 'Close', 'Traded Volume']})

fetch_quandl("EURONEXT/FORDP",
             fields={'Open': 'open', 'High': 'high', 'Low': 'low', 'Last': 'close', 'Volume': 'volume',
                     'NDHighLow': 'NDHighLow'},
             start_date="2014-02-14",
             # end_date="2018-12-31",
             qopts={'columns': ['Open', 'High', 'Low', 'Last', 'Volume']}
             )
fetch_quandl("FSE/DAI_X",
             fields={'Open': 'open', 'High': 'high', 'Low': 'low', 'Close': 'close', 'Traded Volume': 'volume',
                     'NDHighLow': 'NDHighLow'},
             start_date="2001-09-17",
             # end_date="2018-12-31",
             qopts={'columns': ['Open', 'High', 'Low', 'Last', 'Volume']}
             )

fetch_iextrading("TSLA")
fetch_iextrading("GM")
fetch_iextrading("RACE")

# data layout
# Index, Open, High, Low, Close, Change, _6,      Turnover, _8,            _9,                _10,           NDHighLow
# Date   Open  High  Low  Close  Change  TradeVol Turnover  LastPriceofDay Daily Traded Units Daily Turnover NDHighLow
