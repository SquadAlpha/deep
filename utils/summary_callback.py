import os
from datetime import datetime as datetime

from tensorflow.contrib import summary as sm
import tensorflow as tf
from tensorflow.python.ops.summary_ops_v2 import SummaryWriter


class SummaryCallback(object):
    """Abstract base class used to build new callbacks.

    Attributes:
        params: dict. Training parameters
            (eg. verbosity, batch size, number of epochs...).
        model: instance of `keras.models.Model`.
            Reference of the model being trained.

    The `logs` dictionary that callback methods
    take as argument will contain keys for quantities relevant to
    the current batch or epoch.

    Currently, the `.fit()` method of the `Sequential` model class
    will include the following quantities in the `logs` that
    it passes to its callbacks:

        on_epoch_end: logs include `acc` and `loss`, and
            optionally include `val_loss`
            (if validation is enabled in `fit`), and `val_acc`
            (if validation and accuracy monitoring are enabled).
        on_batch_begin: logs include `size`,
            the number of samples in the current batch.
        on_batch_end: logs include `loss`, and optionally `acc`
            (if accuracy monitoring is enabled).
    """
    writer: SummaryWriter

    def __init__(self, file_writer, model, path):
        self.validation_data = None
        self.model = model
        self.writer = file_writer
        self.save_path = path
        self.writer.set_as_default()
        self.writer.init()
        self.epoch = "-1"

    def scalar(self, name: str, value, family: str = None, step: int = None):
        with self.writer.as_default(), tf.contrib.summary.always_record_summaries():
            sm.scalar(name, value, family=family, step=step)
        self.flush()
        pass

    def flush(self):
        with self.writer.as_default(), tf.contrib.summary.always_record_summaries():
            sm.flush(self.writer)
            # self.writer.flush()
        pass

    def close(self):
        self.writer.close()
        pass

    def set_params(self, params):
        self.params = params

    def set_model(self, model):
        self.model = model

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch = str(epoch)
        for key, value in logs.items():
            self.scalar(key, value, "epoch", epoch)
        self.flush()
        pass

    def on_epoch_end(self, epoch, logs=None):
        self.epoch = "-1"
        for key, value in logs.items():
            self.scalar(key, value, "epoch", epoch)
        self.flush()
        os.makedirs(self.save_path+"/checkpoints", exist_ok=True)
        self.model.save(self.save_path + "/checkpoints/" + self.get_time() + ".ckpt", include_optimizer=False)
        pass

    def on_batch_begin(self, batch, logs=None):
        for key, value in logs.items():
            self.scalar(key, value, "batch/{0}".format(self.epoch), batch)
        self.flush()
        pass

    def on_batch_end(self, batch, logs=None):
        for key, value in logs.items():
            self.scalar(key, value, "batch/{0}".format(self.epoch), batch)
        self.flush()
        pass

    def get_time(self):
        return datetime.now().strftime("%m-%d_%H-%M-%S")

    def on_train_begin(self, logs=None):
        for key, value in logs.items():
            self.scalar(key, value, "train")
        self.flush()
        pass

    def on_train_end(self, logs=None):
        for key, value in logs.items():
            self.scalar(key, value, "train")
        self.flush()
        pass
