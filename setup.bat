IF EXIST "venv\Scripts\activate " (
  echo "File found!"
) ELSE (
  echo "creating venv"
    pip install --upgrade virtualenv
    virtualenv venv
)

venv\Scripts\activate                   #enable virtualenv
pip install --upgrade pip               #update pip
pip install -r requirements.txt       #install deps