import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras import layers
import trainer as tn

trainer = tn.Trainer()

input_shape, output_class_len = trainer.get_data_info()

model = keras.Sequential()
model.add(layers.Dense(128, name="Dense1", input_shape=input_shape, activation=tf.nn.relu))
model.add(layers.Dense(128, name="Dense2", activation=tf.nn.relu))
model.add(layers.Dense(output_class_len, name="Dense3"))

# For a multi-class classification problem
model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=0.001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

trainer.do_training(model)
