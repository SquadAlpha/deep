#!/bin/bash

if [ -f venv/bin/activate ]; then
    echo "File found!"
else
    echo "creating venv"
    pip install --user --upgrade virtualenv
    virtualenv -p python3 venv
fi

source venv/bin/activate            #enable virtualenv
pip install --upgrade pip           #update pip
pip install -r requirements.txt    #install deps