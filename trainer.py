# TensorFlow and tf.keras
import datetime

import tensorflow as tf
from tensorflow.python.keras.callbacks import History

from utils.data_import import DataImporter
from utils import save_manager as sm


class Trainer(object):

    def __init__(self):
        super().__init__()
        self.data = None
        self.data_length = 0
        self.class_names = ['Lower', 'Same', 'Higher']
        self.di = DataImporter()

    def load_data(self):
        if self.data is None:
            self.data_length, self.data = self.di.get_training_set()
        pass

    def get_data_info(self):
        tf.enable_eager_execution()
        self.load_data()
        return self.data.output_shapes[0], len(self.class_names)

    def do_training(self, model):
        now = datetime.datetime.now()

        tf.random.set_random_seed(9101999)  # set random seed for easier reproduction

        self.load_data()

        epochs = 10 # TODO set to 100
        batch_size = int(self.data_length/100)
        steps_per_epoch = self.data_length

        # training_dataset
        print(self.data)
        print(self.data.output_shapes)

        tf.contrib.summary.record_summaries_every_n_global_steps(1)
        tf.contrib.summary.always_record_summaries()

        print("in:" + str(model.input_shape))
        print(model.summary())
        print("out:" + str(model.output_shape))

        cb = sm.get_callbacks(model, now)

        hist: History = model.fit(x=self.data, batch_size=batch_size, epochs=epochs, steps_per_epoch=steps_per_epoch,
                                  verbose=1,
                                  callbacks=cb)

        sm.save_model(model, hist, now)
        cb[0].close()
